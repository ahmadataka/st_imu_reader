%% Code to run model and sends velocity control to EPOS

patient_id = 'Test_20012021';
%Session

% Initalise Variables 
%%
close all
% clear all
%clearvars -except Traj
clc

global s addresses N_devices buffLen pckLen ...
    QUAT ij_prev RxBuff_prev Vt Vs Ve Vts Vte Vse R10 R20 R30 count_imu...
    counter e ePrev model link_1 link_2 link_3 Theta1 Theta2 cval rot...
    N_samples ShdFlexAng ShdAbdAng ShdProAng ElbFlexAng ElbZang Phi1 Phi2 Phi3 patient_id ...
    Zeta_flexion Zeta_adduction Zeta_rot

%load('C:\Users\PC\OneDrive - Nanyang Technological University\IMU\Exosuit\data\patient_Bernardo\Trajectory.mat');
%load('C:\Users\PC\OneDrive - Nanyang Technological University\IMU\Exosuit\data\patient_Bernardo\words_mat_final.mat');
% Traj_new = mat(:,2);
% Traj = Traj(:,2,3).*pi/180;

%% IMU variables

% addresses = {'C0:82:26:31:22:48','C0:82:38:31:5E:48'}; % sensor1 address:'C0:82:26:31:22:48', sensor2 address:'C0:82:38:31:5E:48'  
addresses = {'C0:82:26:31:22:48','C0:82:38:31:5E:48','C0:82:1D:30:25:4D'}; % sensor1 address:'C0:82:26:31:22:48', sensor2 address:'C0:82:38:31:5E:48', sensor3 address:'C0:82:39:31:22:48',sensor3_new :'C0:82:1D:30:25:4D'
N_devices = length(addresses);
ij_prev = 0;
RxBuff_prev = [];
counter = 1;
count_imu = 1;
x=1:500000000;
N_samples = length(x);
R10 = eye(3,3);
R20 = eye(3,3);
R30 = eye(3,3);
buffLen = 34; %45%% Size of buffer (bytes)
pckLen = 27; %% Size of 1 set of IMU data (bytes)
e = 0;
ePrev = 0;
link_1 = [0 0 -1]';
link_2 = [0 0 -1]';
link_3 = [0 0 -1]';
Theta1 = 0;
Theta2 = 0;
%Zeta_flexion =0;
% Zeta_adduction=0;
% Zeta_rot=0;

ShdFlexAng =0;
ShdAbdAng=0;
ShdProAng=0;
ElbFlexAng=0;
ElbZang=0;

emg_signal = [];
Tau_val = [];
mval1 = '/val1';
mval2 = '/val2';
mval3 = '/val3';
mval4 = '/val4';
mval5 = '/val5';
mval6 = '/val6';
mval7 = '/val7';
mval8 = '/val8';

cval = 0;
TrajElbow = '/TrajElbow';

%TrajShoulder = '/RefTrajShoulder';
Trig = '/Trigger';
startval = 0;

for i = 1 : N_devices
    QUAT{i} = zeros(4, 1);
    rot{i} = eye(3);
end

%% ESTABLISH SERIAL COMMUNICATION
% instrhwinfo('serial')

if ~isempty(instrfind)
    fclose(instrfind);
    delete(instrfind);
end

s = serial('COM21', 'BaudRate', 115200, 'StopBits', 1);  % 
s.BytesAvailableFcn = @(~,~)kieran_mycallback_quanserrealtime(s);
s.BytesAvailableFcnMode = 'byte';
s.BytesAvailableFcnCount = buffLen;


%%

% Run model
model = 'Control_Kieran_quanser_imu_TTSH'; %'Control_Domenico_quanser_imu';
% model = 'Control_Kieran_quanser_imu_niklaus'; %'Control_Domenico_quanser_imu';
open_system(model);

qc_load_model(model);
qc_start_model(model);

fopen(s); %% Start serial streaming for IMU data
    
disp('START')


count = 1;
counter_new = 1;
t = 0;
play = 1;
loop = 0;
tic;
started = 0;

while(play)
     %t(loop,1) = toc;
     %tic; 
    
    cval1 = get_param([model,'/calib'],'RuntimeObject');
    cval = cval1.InputPort(1).Data;
    sval1 = get_param([model,'/stop'],'RuntimeObject');
    sval = sval1.InputPort(1).Data;
    
    if (sval==1)
        qc_start_model(model);
        fclose(s);
        delete(s);
        play = 0;
        disp('close port');
    end
    
    count = count+1;
    
    if(count>90)
        count=1;
    end
    
%     set_param([model mval1],'Value', num2str(-1*ShdFlexAng));
%     set_param([model mval2],'Value', num2str(-1*ShdAbdAng));
%     set_param([model mval3],'Value', num2str(ShdProAng));
%     set_param([model mval4],'Value', num2str(-1*ElbFlexAng));
%     set_param([model mval5],'Value', num2str(ElbZang));

    %set_param([model TrajElbow],'Value', num2str(Traj_new(counter_new)));
    set_param([model mval1],'Value', num2str(Theta1));
    set_param([model mval2],'Value', num2str(Theta2));
    set_param([model mval3],'Value', num2str(Phi1));
    set_param([model mval4],'Value', num2str(Phi2));
    set_param([model mval5],'Value', num2str(Phi3));
    set_param([model mval6],'Value', num2str(Zeta_flexion));
    set_param([model mval7],'Value', num2str(Zeta_adduction));
    set_param([model mval8],'Value', num2str(Zeta_rot));
    
%     xyz = get_param([model '/Data Store Memory'],'Initial value')
    
    pause(0.00001);
    %counter_new =+1; 
    
    % niklaus code
%     emg_signal = get_param([model '/Scope2'],'RuntimeObject');
    
    
end



