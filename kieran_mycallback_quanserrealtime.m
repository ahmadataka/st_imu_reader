function kieran_mycallback_quanserrealtime(~,~)

global s addresses N_devices buffLen pckLen ...
    QUAT ij_prev RxBuff_prev Vt Vs Ve Vts Vte Vse R10 R20 R30 count_imu...
    counter e ePrev link_1 link_2 link_3 Theta1 Theta2 cval rot...
    N_samples ShdFlexAng ShdAbdAng ShdProAng ElbFlexAng ElbZang Phi1 Phi2 Phi3 ...
    Zeta_flexion Zeta_adduction Zeta_rot
    
    if(cval==1)
        count_imu = 1;
    end
    RxBuff = fread(s,buffLen);
    ij = 1;
    if(ij_prev>(buffLen-pckLen))
        RxBuff = [RxBuff_prev(ij_prev:end);RxBuff];
    end
    buffLen_new = size(RxBuff,1);
    
    while ij <= (buffLen_new-pckLen)
        found_device = 0;
        address = [dec2hex(RxBuff(ij+5), 2), ':', ...
            dec2hex(RxBuff(ij+4), 2), ':', ...
            dec2hex(RxBuff(ij+3), 2), ':', ...
            dec2hex(RxBuff(ij+2), 2), ':', ...
            dec2hex(RxBuff(ij+1), 2), ':', ...
            dec2hex(RxBuff(ij), 2)];

        isMIMU = RxBuff(ij+6);
        for j = 1 : N_devices
            if(strcmp(address, addresses{j}))
                found_device = 1;
                dev_num = j;
            end
        end
        if(found_device == 1)
            
            if(isMIMU == 1)
                timestamp = double(typecast(uint8([RxBuff(ij+7), RxBuff(ij+8) RxBuff(ij+9) RxBuff(ij+10)]), 'uint32'));
                time16bit = double(typecast(uint8([RxBuff(ij+7), RxBuff(ij+8)]), 'uint16'));
                Quat(1) = double(typecast(uint8([RxBuff(ij+11), RxBuff(ij+12) RxBuff(ij+13) RxBuff(ij+14)]), 'int32')) / 1000000; %scalar
                Quat(2) = double(typecast(uint8([RxBuff(ij+15), RxBuff(ij+16) RxBuff(ij+17) RxBuff(ij+18)]), 'int32')) / 1000000; %vx
                Quat(3) = double(typecast(uint8([RxBuff(ij+19), RxBuff(ij+20) RxBuff(ij+21) RxBuff(ij+22)]), 'int32')) / 1000000; %vy
                Quat(4) = double(typecast(uint8([RxBuff(ij+23), RxBuff(ij+24) RxBuff(ij+25) RxBuff(ij+26)]), 'int32')) / 1000000; %vz

                QUAT{dev_num}(1:4) = Quat(1:4);

                if((QUAT{dev_num}(1)==0)&&(QUAT{dev_num}(2)==0)&&(QUAT{dev_num}(3)==0)&&(QUAT{dev_num}(4)==0))
                    rot{dev_num} = eye(3,3);
                else
                    rot{dev_num} = quat2dcm(QUAT{dev_num}');
                end
                
                R11 = eye(3);     
                R12 = eye(3);
                R22 = eye(3);
                R13 = eye(3);
                R13_NR = eye(3);
                
                if(count_imu<=200)
                    R10 = rot{1};
                    R20 = rot{2};
                    R30 = rot{3};
                    
                    R1 = rot{1};
                    R2 = rot{2};
                    disp(count_imu);
                else
                    R1 = rot{1};
                    R2 = rot{2};
                    R3 = rot{3};
                    %SENSOR FRAME WITH RESPECT TO INERTIAL FRAME
                    
%                     R11 = R10' * Vt * Vt' * R1; %% latest version
%                     R22 = R20' * Vs * Vs' * R2;
%                     R33 = R30' * Ve * Ve' * R3;
%                     R12 = R20' * Vts * Vts' * R2;
%                     R13 = R30' * Vte * Vte' * R3;
%                     R23 = R30' * Vse * Vse' * R3; 
                    
                    R11 = R10\R1; % Rotation matrix wrt Initial configuration     
                    R12 = R20\R2; % without using calibration     
                    R13 = R30\R3; 
                    
                    R13_NR = R13;
                    R22 = R12*R11';
                    R13 = R13*R12';
                    %R13 = R13*R11';
                    %R11 = R11*R11';
                    
                end
                l1 = R11 * link_1;
                l2 = R12 * link_2;
                l3 = R13 * link_3;
                l3_NR = R13_NR * link_3;
                
                l20 = link_2;
                l30 = link_3;
                
                l2 = R22 \ [0,0,1]';
                l_trunk = R1 \ [0,0,1]';
                alpha = atan2d(l2(2),l2(1));
                
                Theta1 = atan2d(norm(cross([0,0,1],R13*[0,0,1]')),dot([0,0,1],R13*[0,0,1]')); %Flexion
                Theta2 = atan2d(norm(cross([0,0,1],R13_NR*[0,0,1]')),dot([0,0,1],R13_NR*[0,0,1]')); %Forearm, gravity
                Phi1 = atan2d(norm(cross([0,0,-1],R22*[0,0,-1]')),dot([0,0,-1],R22*[0,0,-1]')); %Elevation Shoulder
                Phi2 = 90 + alpha;
                Phi3 = 90 - atan2d(norm(cross([0,0,1],R2*[0,0,1]')),dot([0,0,1],R2*[0,0,1]')); %
                
                %Trunk
                Zeta_flexion = -90 + atan2d(norm(cross([0,0,-1],(R1)*[0,0,-1]')),dot([0,0,-1],(R1)*[0,0,-1]'));%atan2d(norm(cross(l4,l6)),dot(l4,l6));
                Zeta_adduction = -90 + atan2d(norm(cross([0,-1,0],R1*[0,0,-1]')),dot([0,-1,0],R1*[0,0,-1]'));%atan2d(norm(cross(l1,l9)),dot(l1,l9));
                Zeta_rot = -90 +atan2d(l_trunk(2),l_trunk(1));
                
%                 [gam11,bet11,alp11] = rot2eu1(R11); % Euler angles: Shoulder joint 
%                 [gam12,bet12,alp12] = rot2eu1(R12); % Euler angles: Shoulder joint 
%                 [gam13,bet13,alp13] = rot2eu1(R13); % Euler angles: Shoulder joint
% 
%                 ShdFlexAng = gam12;
%                 ShdAbdAng = bet12;
%                 ShdProAng = (alp12+gam12)+sqrt((gam12^2)+(bet12^2));
%                 
%                 ElbFlexAng = gam13;
%                 ElbZang = atan2d(norm(cross(l30,l3_NR)),dot(l30,l3_NR));

                
%                 Theta1 = atan2d(norm(cross(l1,l2)),dot(l1,l2));
%                 Theta2 = atan2d(norm(cross(l2,l3)),dot(l2,l3));
                

%                 Theta1 = atan2d(norm(cross(l20,l2)),dot(l20,l2));
%                 Theta2 = atan2d(norm(cross(l30,l3)),dot(l30,l3));
%                 Theta2 = Theta2 - Theta1;
                
%                 UPP_ARM(1,:) = [UPP_ARM(1,2:N_samples) Theta1];
%                 UPP_ARM(2,:) = [UPP_ARM(2,2:N_samples) Theta1];
%                 LOW_ARM(1,:) = [LOW_ARM(1,2:N_samples) Theta2];
%                 LOW_ARM(2,:) = [LOW_ARM(2,2:N_samples) Theta2];
%                 
%                 set(plotUPP(1,:), 'Ydata', UPP_ARM(1,:));
%                 set(plotUPP(2,:), 'Ydata', UPP_ARM(2,:));
%                 set(plotLOW(1,:), 'Ydata', LOW_ARM(1,:));
%                 set(plotLOW(2,:), 'Ydata', LOW_ARM(2,:));
              
                count_imu = count_imu + 1;
            end
            ij = ij + pckLen;
        elseif(found_device == 0)
            ij = ij + 1;
        end
    end
    ij_prev = ij;
    RxBuff_prev = RxBuff;

    counter = counter+1;
     
    ePrev = e;
end