#!/usr/bin/env python

import math
import serial
import string
import numpy as np
# Buffer length
addresses = ['C0:82:26:31:22:48','C0:82:38:31:5E:48','C0:82:1D:30:25:4D']
add_len = 6
N_devices = len(addresses)
count_imu = 1
# x=1:500000000;
# N_samples = length(x);
# R10 = eye(3,3);
# R20 = eye(3,3);
# R30 = eye(3,3);
buffLen = 34
pckLen = 27
e = 0
ePrev = 0

def parse_data(data):
    showing = 0
    showing_index = 0
    RxBuff = [0]*buffLen
    RxBuff_prev = []
    ij_prev = 0
    counter = 1

    # if(cval==1)
    #     count_imu = 1;
    # end
    for i in range(0, buffLen):
        # print ord(data[i])
        # print hex(ord(data[i]))
        RxBuff[i] = ord(data[i])
        # if(RxBuff[i]==0xC0):
        #     showing = 1
        # if(showing == 1):
        #     print(hex(RxBuff[i]))
        #     showing_index = showing_index +1
        #     if(showing_index == 6):
        #         showing = 0
    # # RxBuff = data
    ij = 0
    if(ij_prev>(buffLen-pckLen)):
        RxBuff = RxBuff_prev[ij_prev:len(RxBuff_prev)].extend(RxBuff)
    buffLen_new = len(RxBuff)
    
    while ij <= (buffLen_new-pckLen):
        found_device = 0
        address = ''
        for i in range(0, add_len):
            address = address  + "{:02X}".format(RxBuff[ij+add_len-1-i])
            if(i<add_len-1):
                address = address +':'
        # address = str(hex(RxBuff[ij+5]))+':'+str(hex(RxBuff[ij+4]))+':'+str(hex(RxBuff[ij+3]))+':'+str(hex(RxBuff[ij+2]))+':'+str(hex(RxBuff[ij+1]))+':'+str(hex(RxBuff[ij]))
        isMIMU = RxBuff[ij+6]
        for j in range(0,N_devices):
            if(address==addresses[j]):
                found_device = 1
                dev_num = j
        quat = []
        if(found_device == 1):
            if(isMIMU == 1):
                print(dev_num)
                print("FOUND")
                timestamp = np.array([RxBuff[ij+7], RxBuff[ij+8], RxBuff[ij+9], RxBuff[ij+10]], dtype=np.int8)
                timestamp.dtype = np.uint32
                timestamp = float(timestamp)
                print(timestamp)
                time16bit = np.array([RxBuff[ij+7], RxBuff[ij+8]], dtype=np.int8)
                time16bit.dtype = np.uint16
                time16bit = float(time16bit)
                print(time16bit)
                for ind_quat in range(0, 4):
                    quat_comp = np.array([RxBuff[ij+11+4*ind_quat], RxBuff[ij+12+4*ind_quat], RxBuff[ij+13+4*ind_quat], RxBuff[ij+14+4*ind_quat]], dtype=np.int8)
                    quat_comp.dtype = np.int32
                    quat_comp = float(quat_comp)/1000000.0
                    quat.append(quat_comp)
                print(quat)

#                 QUAT{dev_num}(1:4) = Quat(1:4);

#                 if((QUAT{dev_num}(1)==0)&&(QUAT{dev_num}(2)==0)&&(QUAT{dev_num}(3)==0)&&(QUAT{dev_num}(4)==0))
#                     rot{dev_num} = eye(3,3);
#                 else
#                     rot{dev_num} = quat2dcm(QUAT{dev_num}');
#                 end
                
#                 R11 = eye(3);     
#                 R12 = eye(3);
#                 R22 = eye(3);
#                 R13 = eye(3);
#                 R13_NR = eye(3);
                
#                 if(count_imu<=200)
#                     R10 = rot{1};
#                     R20 = rot{2};
#                     R30 = rot{3};
                    
#                     R1 = rot{1};
#                     R2 = rot{2};
#                     disp(count_imu);
#                 else
#                     R1 = rot{1};
#                     R2 = rot{2};
#                     R3 = rot{3};
#                     %SENSOR FRAME WITH RESPECT TO INERTIAL FRAME
                    
# %                     R11 = R10' * Vt * Vt' * R1; %% latest version
# %                     R22 = R20' * Vs * Vs' * R2;
# %                     R33 = R30' * Ve * Ve' * R3;
# %                     R12 = R20' * Vts * Vts' * R2;
# %                     R13 = R30' * Vte * Vte' * R3;
# %                     R23 = R30' * Vse * Vse' * R3; 
                    
#                     R11 = R10\R1; % Rotation matrix wrt Initial configuration     
#                     R12 = R20\R2; % without using calibration     
#                     R13 = R30\R3; 
                    
#                     R13_NR = R13;
#                     R22 = R12*R11';
#                     R13 = R13*R12';
#                     %R13 = R13*R11';
#                     %R11 = R11*R11';
                    
#                 end
#                 l1 = R11 * link_1;
#                 l2 = R12 * link_2;
#                 l3 = R13 * link_3;
#                 l3_NR = R13_NR * link_3;
                
#                 l20 = link_2;
#                 l30 = link_3;
                
#                 l2 = R22 \ [0,0,1]';
#                 l_trunk = R1 \ [0,0,1]';
#                 alpha = atan2d(l2(2),l2(1));
                
#                 Theta1 = atan2d(norm(cross([0,0,1],R13*[0,0,1]')),dot([0,0,1],R13*[0,0,1]')); %Flexion
#                 Theta2 = atan2d(norm(cross([0,0,1],R13_NR*[0,0,1]')),dot([0,0,1],R13_NR*[0,0,1]')); %Forearm, gravity
#                 Phi1 = atan2d(norm(cross([0,0,-1],R22*[0,0,-1]')),dot([0,0,-1],R22*[0,0,-1]')); %Elevation Shoulder
#                 Phi2 = 90 + alpha;
#                 Phi3 = 90 - atan2d(norm(cross([0,0,1],R2*[0,0,1]')),dot([0,0,1],R2*[0,0,1]')); %
                
#                 %Trunk
#                 Zeta_flexion = -90 + atan2d(norm(cross([0,0,-1],(R1)*[0,0,-1]')),dot([0,0,-1],(R1)*[0,0,-1]'));%atan2d(norm(cross(l4,l6)),dot(l4,l6));
#                 Zeta_adduction = -90 + atan2d(norm(cross([0,-1,0],R1*[0,0,-1]')),dot([0,-1,0],R1*[0,0,-1]'));%atan2d(norm(cross(l1,l9)),dot(l1,l9));
#                 Zeta_rot = -90 +atan2d(l_trunk(2),l_trunk(1));
                
# %                 [gam11,bet11,alp11] = rot2eu1(R11); % Euler angles: Shoulder joint 
# %                 [gam12,bet12,alp12] = rot2eu1(R12); % Euler angles: Shoulder joint 
# %                 [gam13,bet13,alp13] = rot2eu1(R13); % Euler angles: Shoulder joint
# % 
# %                 ShdFlexAng = gam12;
# %                 ShdAbdAng = bet12;
# %                 ShdProAng = (alp12+gam12)+sqrt((gam12^2)+(bet12^2));
# %                 
# %                 ElbFlexAng = gam13;
# %                 ElbZang = atan2d(norm(cross(l30,l3_NR)),dot(l30,l3_NR));

                
# %                 Theta1 = atan2d(norm(cross(l1,l2)),dot(l1,l2));
# %                 Theta2 = atan2d(norm(cross(l2,l3)),dot(l2,l3));
                

# %                 Theta1 = atan2d(norm(cross(l20,l2)),dot(l20,l2));
# %                 Theta2 = atan2d(norm(cross(l30,l3)),dot(l30,l3));
# %                 Theta2 = Theta2 - Theta1;
                
# %                 UPP_ARM(1,:) = [UPP_ARM(1,2:N_samples) Theta1];
# %                 UPP_ARM(2,:) = [UPP_ARM(2,2:N_samples) Theta1];
# %                 LOW_ARM(1,:) = [LOW_ARM(1,2:N_samples) Theta2];
# %                 LOW_ARM(2,:) = [LOW_ARM(2,2:N_samples) Theta2];
# %                 
# %                 set(plotUPP(1,:), 'Ydata', UPP_ARM(1,:));
# %                 set(plotUPP(2,:), 'Ydata', UPP_ARM(2,:));
# %                 set(plotLOW(1,:), 'Ydata', LOW_ARM(1,:));
# %                 set(plotLOW(2,:), 'Ydata', LOW_ARM(2,:));
              
#                 count_imu = count_imu + 1;
#             end
            ij = ij + pckLen
        elif(found_device == 0):
            ij = ij + 1


    ij_prev = ij
    RxBuff_prev = RxBuff

    counter = counter+1
     
    ePrev = e
        
# Initialize the serial class
ser = serial.Serial()
# Define the serial port
ser.port = "/dev/ttyACM0"
# Set the baudrate
ser.baudrate = 115200
# Set the timeout limit
ser.timeout = 1
# Open the serial
ser.open()

# Initialize the flag used to check the availability of the received serial data
flag_sent = 0

while True:
    try:
        # Read data from the Serial
        data = ser.read(buffLen)
        # data = ser.readline()
        # print "len:"
        # print len(data)
        if(len(data)>=buffLen):
            parse_data(data)
        
        # # Split the serial data into several words separated by space
        # words = data.split()
        # # Get the length of the data array
        # word_len = len(words)

    except KeyboardInterrupt:
        ser.close()
        print("Bye")


